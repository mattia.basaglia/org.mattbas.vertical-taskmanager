Default task manager but larger icons when vertical.


## Installation

```bash
# Install
kpackagetool5 -i org.mattbas.vertical-taskmanager

# Update
kpackagetool5 -u org.mattbas.vertical-taskmanager

# Remove
kpackagetool5 -r org.mattbas.vertical-taskmanager

# Restart Plasma shell
killall plasmashell && plasmashell &
```

